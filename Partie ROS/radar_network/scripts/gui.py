#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import matplotlib.pyplot as plt

def callback(data):
    #rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    if data.data.startswith("2014"):
    # ADC
        x=[]
        y1=[]
        y2=[]
        for i in range(128):    
		    x.append(i);
		    y1.append(int(data.data[3+i*2], 16) * pow(2, 8) + int(data.data[4+i*2], 16));
		    y2.append(int(data.data[256 + 3+i*2], 16) * pow(2, 8) + int(data.data[256 + 4+i*2], 16));
        
        plt.plot(x, y1, x, y2)
        plt.show()
	#elif data.startswith("1973"):
			# Target
	#elif data.startswith("1984"):
			# Range
	#elif data.startswith("1995"):
			# Doppler
		
    
def subscriber():

    
    rospy.init_node('subscriber', anonymous=True)

    rospy.Subscriber("Data", String, callback)
 
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    subscriber()

