#!/usr/bin/env python
import socket
import binascii
import rospy
from std_msgs.msg import String


def radar_UDP():
    pub = rospy.Publisher('Data', String, queue_size=10)  # Publisher pour publier les données brutes reçues en UDP
    rospy.init_node('radar_UDP', anonymous=True) 
    rate = rospy.Rate(10) # 10hz
    
     
    UDP_IP = "192.168.0.20" 
    UDP_PORT = 40001

    sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP, UDP_PORT))
    while not rospy.is_shutdown():    
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
		#print "received message:", binascii.b2a_hex(data)
        pub.publish(binascii.b2a_hex(data)) #publie les trames sous forme hexadecimale
        rate.sleep()

if __name__ == '__main__':
    try:
        radar_UDP()
    except rospy.ROSInterruptException:
        pass
