#!/usr/bin/env python

import socket
import binascii
import rospy
import matplotlib.pyplot as plt
import matplotlib.pyplot as pltbis
from std_msgs.msg import String
from pylab import figure, axes, pie, title, show
import numpy.fft
import os
from PIL import Image
#import imageio
import matplotlib.image as mpimg
import Tkinter as tk #imports tkinter

dist = []
vitesse = []
rcs = []
i=0x00
j=0

def callback(data):
	global i
	global j
	j = 1
	rospy.loginfo(i)
	pub = rospy.Publisher('data_treatment', String, queue_size=10)
	trame = data.data
	ident = trame[0:4]
	target = trame[16:len(trame)]
	i=0x00
	if ident == "1973":
		nb_target = int(trame[14:16],16)
		rospy.loginfo(target)
		while i != nb_target:
			vitesse.append(int(target[4+i*16:8+i*16],16))
			dist.append(int(target[0+i*16:4+i*16],16))
			if int(target[4+i*16:8+i*16],16) > 32767:
				vitesse.append(~int(target[4+i*16:8+i*16],16))								
			rcs.append(int(target[12+i*16:16+i*16],16))			
			i = i + 1
	j = 0
		
    
def traitement():
	global j
	rospy.init_node('traitement', anonymous=True)
	sub_raw_data = rospy.Subscriber("raw_data", String, callback)

	nb = input('Choose a number')
	print ('Number%s \n' % (nb))
	while j == 0:   
		sub_raw_data.unregister()
		j = 1
	color = 'ro'
	#vit=numpy.fft.fftshift(vitesse)
	plt.axis([0x0000,0x0020,0x0000,0x000a])
	plt.plot(vitesse, dist, color)
	plt.savefig('/home/pal/darknet/image_tests/test_tempsreel/dataset'+ str(nb) +'.jpg', dpi = 100)
	plt.close("all")
	os.system('cd; cd /home/pal/darknet/; sudo ./darknet detector test data_radar/obj.data yolo-obj.cfg nouvellematricedepoids.weights image_tests/test_tempsreel/dataset'+str(nb) + '.jpg')
	fenetre = tk.Tk()
	photo = tk.PhotoImage(file="/home/pal/darknet/predictions.png")
	label = tk.Label(fenetre, image=photo)
	label.pack()

	fenetre.mainloop()
	

if __name__ == '__main__':
	traitement()
