#!/usr/bin/env python

import socket
import binascii
import rospy
from std_msgs.msg import String
import math
import matplotlib.pylab as plt
import numpy as np
from std_msgs.msg import Float64
p = 0


def callback(data):
	global tab_mod
	global p
	#global donnee
	donnee = []
	p = 1
	chirp_number = 0
	size = 0
	re = []
	im = []
	module=[]
	trame = data.data
	chirp_number = trame[4:6]
	size = len(trame)
	i = 0
	if trame[0:4] == "1995":
		while i < 128:
			if int(trame[6+i*4:10+i*4],16) < 32767 or int(trame[10+i*4:14+i*4],16) < 32767:
				re.append(int(trame[6+i*4:10+i*4],16))
				im.append(int(trame[10+i*4:14+i*4],16))
				module.append(float(math.sqrt(re[i]*re[i] + im[i]*im[i])))
				pub = rospy.Publisher('data', Float64, queue_size=10)
				pub.publish(module[i])
				i=i+1	
	p = 0

def traitement():
	global p
	global tab_mod
	global donnee
	rospy.init_node('traitement', anonymous=True)
	rospy.Subscriber("raw_data", String, callback)
	rospy.spin()


if __name__ == '__main__':
    traitement()
