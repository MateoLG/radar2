#!/usr/bin/env python

import socket
import binascii
import rospy
from std_msgs.msg import String
import math
import matplotlib.pylab as plt
#from PIL import Image
import numpy as np
tab_mod = [[]]
p = 0
donnee = []

def callback(data):
	global tab_mod
	global p
	global donnee
	p = 1
	chirp_number = 0
	size = 0
	re = []
	im = []
	module=[]
	trame = data.data
	chirp_number = trame[4:6]
	size = len(trame)
	i = 0
	#rospy.loginfo(trame[0:4])
	if trame[0:4] == "1995":
		while i < 128:
			#print int(trame[6+i*4:10+i*4],16)
			if int(trame[6+i*4:10+i*4],16) < 32767 or int(trame[10+i*4:14+i*4],16) < 32767:
				re.append(int(trame[6+i*4:10+i*4],16))
				im.append(int(trame[10+i*4:14+i*4],16))
				module.append(float(math.sqrt(re[i]*re[i] + im[i]*im[i])))
				i=i+1
		print module
		donnee = np.array(module)
		shape =(8,16)
		donnee.reshape(shape)
		#for i in range(11):
		#	tab_mod.append([])
		#	for j in range(12):
		#		if (i*11+j) >= len(module):
					#print "========="
					#print(i*11+j)
		#			tab_mod[-1].append(float(1))
		#		else:
		#			tab_mod[-1].append(float(module[i*11+j]))
	print "*******************"
	print donnee
	p = 0
	#im = Image.fromarray(tab_mod)
	#im.show()


def traitement():
	global p
	global tab_mod
	global donnee
	rospy.init_node('traitement', anonymous=True)
	rospy.Subscriber("raw_data", String, callback)

	while True:
		if p ==0:
			print len(donnee)
			if len(donnee) > 0:
				print donnee
				matrice = np.matrix(donnee)
				#matrice = numpy.matrix(matricetest)
				fig = plt.figure()
				plt.axis([0x0000,0xFFFF,0x0000,0xFFFF])
				plt.imshow(matrice)
				#plt.colorbar()
				plt.show()


if __name__ == '__main__':
    traitement()
