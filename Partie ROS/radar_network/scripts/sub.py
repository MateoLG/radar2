#!/usr/bin/env python

import rospy
from std_msgs.msg import String


def callback(data):
    rospy.loginfo("data %s", data.data)
    trame=data.data
    f=open("data2.txt","a")
    config = "PIETON" ######## A MODIFIER EN FONCTION DES CAS ##########
    f.write( trame + "		" + config + "\n")
    f.close()
    
def listener():

    rospy.init_node('sub', anonymous=True)
    rospy.Subscriber("data_treatment", String, callback)
    rospy.spin()
    
if __name__ == '__main__':
    try:
        listener()
    except rospy.ROSInterruptException:
        pass

