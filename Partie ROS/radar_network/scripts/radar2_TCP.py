#!/usr/bin/env python

import socket


TCP_IP = '192.168.0.30'
TCP_PORT = 23
BUFFER_SIZE = 1024
MESSAGE = "03WD 0\r\n"	# commande pour parametrer le radar en doppler
						# le parametre est code en dur, on pourrait utiliser un service ROS par exemple
						# pour naviguer entre les modes

while True:  # la boucle sert a envoyer en continue la commande, sans cela le comportement est aleatoire
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)	# cree le socket
	s.connect((TCP_IP, TCP_PORT)) #  connexion
	s.send(MESSAGE + "\n")
	data = s.recv(BUFFER_SIZE)
	#s.close()

	print "received data:", data
