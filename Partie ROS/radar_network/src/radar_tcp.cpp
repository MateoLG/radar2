#include <ros/ros.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <cstdlib>

int main(int argc, char **argv)
{
    struct sockaddr_in serv_addr;
    int socket_TCP, numPort, n;
    char buffer[256];

    ros::init(argc, argv, "radar_tcp");
    ros::NodeHandle nh;

    ros::Rate loop_rate(100);

    socket_TCP = socket(AF_INET, SOCK_STREAM, 0); // SOCK_DGRAM => TCP ; SOCK_STREAM => TCP
    if (socket_TCP < 0)
    {
        ROS_INFO("Error opening TCP socket");
        ros::shutdown();
    }
    ROS_INFO("TCP Socket opened");

    if (argc != 3)
    {
        ROS_INFO("Wrong number of arguments: you need to put ip address and port number");
    }
    inet_aton(argv[1], &(serv_addr.sin_addr));
    numPort = atoi(argv[2]);

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(numPort);

    if(connect(socket_TCP, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0)
    {
        ROS_INFO("Error connecting to server");
        ros::shutdown();
    }
    ROS_INFO("TCP Socket connected to server");

    while (ros::ok())
    {
        strcpy(buffer, "05WD 1\r\n");
        n = write(socket_TCP, buffer, strlen(buffer));
        if (n < 0)
        {
            ROS_INFO("Error writing to socket");
            ros::shutdown();
        }

        n = read(socket_TCP, buffer, strlen(buffer));
        if (n < 0)
        {
            ROS_INFO("Error reading socket");
            ros::shutdown();
        }

        ROS_INFO("TCP Socket read");
        ROS_INFO("buffer:%s\n", buffer);

        loop_rate.sleep();
        ros::spinOnce();
    }


  return 0;
}

