#################### Application des dispositifs radars et du deeplearning � la robotique de services ####################
####################  GROUPE N�02  ####################

## AUTEURS:
 - Jean Gallois
 - Mat�o Leclet-Guillot
 - Bastien Poizat
 
## GIT:
 - D�pot GIT 1
 - D�pot GIT 2
 
## YOUTUBE:
 - https://youtu.be/COjWF2a8M_o

## PROBLEMATIQUE DU PROJET:
 - Prendre en main le Radar Infineon
 - L'adapter � des probl�matiques indoor
 - Mettre en place un deep learning

## NIVEAU D'ABOUTISSEMENT DU PROJET
# DONE:
 - Structure du deep learning mise en place et entrain� pour les pi�tons (Darknet)
 - Communication avec le radar fonctionnelle
 - Code ROS mis en place

# BUGS:
 - Code embarqu� du radar peu adapt� au contexte indoor

# TODO:
 - Modification du code embarqu� pour adapter au contexte indoor
 - Cr�ation d'un dataset et entrainement de la machine pour d'autres cas que les pi�tons
 - Am�lioration du code ROS (par exemple un service pour contr�ler le TCP)
 - Actualisation dynamique du graphique g�n�r� sous ROS


## MATERIEL ET LOGICIEL UTILISE
# MATERIEL
 - Radar Aurix
 - C�ble Ethernet
 - PC Linux avec ROS

# LOGICIEL
 - ROS
 - Qt
 - GUI fournie par Mayeul Jeannin

## ABORESCENCE DES FICHIERS
 - README.txt : Ce fichier
 - PROJET_2 TITRE_DU_PROJET.pptx
 - [VIDEOS]:
	- nom_explicite_1.* : Tel contenu
	- nom_explicite_2.* : Tel autre contenu
 - [nom_d'un_1er_sous_projet_] : par ex projet Keil / Mbed / Visual Studio / ...
	- [blabla]:
		- fichier_x
		- fichier_y
		- fichier_z
	-
	-

#######################################################




#############  PROCEDURE D'INSTALLATION  ##############

(Si installation sp�cifique)
Pr�ciser point potentiellement blocants

#######################################################




##############  MISE EN ROUTE GLOBALE  ################


- Mise en route de la partie ROS

- Mise en route de la partie Deep learning:
	- Lancer la GUI
	- Onglet FFT

#######################################################



############  MISE EN ROUTE ROS ##############

 - ROS:
	- Run radar2_udp.py pour r�cup�rer le flux udp en provenance du radar
	- Run radar2_TCp.py pour changer le type de trame
	- Ecouter le topic /Data

#######################################################



############  MISE EN ROUTE DEEP_LEARNING/DARKNET ##############

 - Deeplearning
	- R�cup�ration d'une image � partir de la GUI
	- Lancer la d�tection avec la commande ./darknet detect cfg/yolo.cfg yolo.weights data_radar/NOM_IMAGE.jpg

#######################################################
